#!/usr/bin/env python3

import requests
import argparse
from urllib.parse import urlencode
from pathlib import Path
from common.common import load_config, local_store, login


api = {}
config = load_config()
force = False
verbose = False

if __name__ == '__main__':
### TBD

# ~ input args
# ~ help

    # Parse input params
    parser = argparse.ArgumentParser(description='Process api reuests.')
    parser.add_argument('-f, --force', default=False, action='store_true', dest='force',
                        help='force load request (Do not use local storage)')
    parser.add_argument('-v, --verbose', default=False, action='store_true', dest='verbose',
                        help='increase output verbosity')
    parser.add_argument('-l', metavar='cz|sk', type=str, default='cz', dest='lang',
                        help='lang')
    group1 = parser.add_mutually_exclusive_group(required=True)
    group1.add_argument('-n', metavar='Jmeno', type=str, dest='name',
                        help='name')
    group1.add_argument('-d', metavar='DDMM', type=int, dest='date',
                        help='date')
    args = parser.parse_args()

    force = args.force
    verbose = args.verbose
    
    api['name'] = 'cz-svatky'
    # store index 
    api['index'] = ''
    api['params'] = {}
    
    # Check auth type
    if config[api['name']]['auth'] is not None and config[api['name']]['auth'] != 'None':
        if config[api['name']]['auth'] == 'api_key':
            api['params'].update({config[api['name']]['auth']: config[api['name']]['api_key']})
        else:
            print('login')
            print(config[api['name']]['auth'])
            print(type(config[api['name']]['auth']))
            login()

    
    # Init local storage
    storage = local_store(api['name'])
    storage_data = storage.load()
    if verbose:
        print(storage.getName())
        print(storage_data)

    # Prepare query
    qstr = ''       # with possilbe auth
    params = {}     # input params
    vars_dict = vars(args)
    for param in ['lang', 'date', 'name']:
        if vars_dict[param] is not None:
            params.update({param: vars_dict[param]})

    api['index'] = urlencode(params)
    params.update(api['params'])
    qstr = urlencode(params)
    if verbose:
        print('Params: %s' % params)
        print('Query string: %s' % qstr)
    
    if storage_data is not None and api['index'] in storage_data.keys() and not force:
        print(storage_data[api['index']])
        if verbose:
            print('Project %s and query %s: Using local storage' % (api['name'], api['index']))
    else:
        # prepare request
        prep = requests.Request('GET',config[api['name']]['url'])
        prep.prepare()
        prep.url += '?%s' % qstr
        if verbose:
            print(prep.url)
        # get request
        r = requests.get(prep.url)
        if r.status_code != 200:
            print('Request failed with error code %s' % r.status_code)
            exit(2)
        if verbose:
            print(r.status_code)
            print(r.headers['content-type'])
            print(r.encoding)
            print(r.json())
            
        # print and store received data    
        url_data = {}
        print(r.json())
        url_data[api['index']] = r.json()
        storage.save(url_data)
