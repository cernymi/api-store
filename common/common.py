#!/usr/bin/env python3
import os
import json
import getpass
import numpy as np

def load_config():
    with open('conf' +  os.sep + 'default.json', 'r') as config_default:
        config = json.load(config_default)
    return config

# ~ def load_data():
    # ~ with open('store/store_default.json', 'r') as src_file:
        # ~ data = json.load(src_file)
    # ~ return data

# ~ def save_data(data):
    # ~ with open('store/store_default.json', 'w') as dest_file:
        # ~ json.dump(data, dest_file)

def login():
    user = input("Username [%s]: " % getpass.getuser())
    if not user:
        user = getpass.getuser()
    password = getpass.getpass('Enter password: ')
    return user, password

class local_store:
    def __init__(self, store_name):
        self.store_name = store_name
    
    def getName(self):
        return self.store_name
    
    def load(self):
        try:
            src_file = open('store' + os.sep + self.store_name + '.json', 'rt')
            data = json.load(src_file)
            # ~ data = np.load('store' + os.sep + 'store_default.json')
            return data
        # ~ except (FileNotFoundError,  json.decoder.JSONDecodeError) as e:
        except (FileNotFoundError) as e:
            pass

    def save(self, req_data):
        if req_data is not None:
            try:          
                write_data = {}
                file_data = self.load()
                if file_data is not None:
                    # ~ print('file not empty')
                    write_data.update(file_data)
                write_data.update(req_data)
                # ~ print(write_data)
                # ~ print('save')

                # ~ np.save('store' + os.sep + 'store_default.json', array) 
                dest_file = open('store' + os.sep + self.store_name + '.json', 'wt')
                json.dump(write_data, dest_file)

            except FileNotFoundError:
                pass

