Api-Store
=========


Api-Store is an open source configurable tool for calling (REST) external apis with possible authentication. Response is made once and stored localy. In case of same request is prioritized response from local store.

Api-Store capabilities include:

  * Definition of external source
  * Authentication supported: login, apikey
  * Store response from source to local storage



ToDo
---------
  * fine tune argument parser
  * shrink func to one script
  * refactor main procedure

Exaples
---------

`python3 api-cz-svatky.py -d 1312`

`python3 api-calendarindex.py -c CZ -y 2018`
